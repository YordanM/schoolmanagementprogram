package schoolmanagement;
import java.util.ArrayList;
import java.util.List;

abstract class Facility 
{
    protected int id;
    protected String name;
    protected String address;
    protected List<Student> students = new ArrayList<Student>(10);

    public abstract double averageGrade();
    public abstract int getTax();
    public abstract double getIncome();
}
