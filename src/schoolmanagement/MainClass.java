package schoolmanagement;

import java.util.*;

public class MainClass {

	// Dictionary for looking up a facility by its ID
	static Map<Integer, Facility> facilities = new HashMap<Integer, Facility>();

	public static void main(String[] args) 
	{
		// Creating schools/universities
		School school1 = new School(1, "School 1", "Address 1");
		School school2 = new School(2, "School 2", "Address 2");
		School school3 = new School(3, "School 3", "Address 3");
		University uni1 = new University(4, "Uni 1", "Address 4");
		University uni2 = new University(5, "Uni 2", "Address 5");

		// Loading students from file
		school1.addStudentFromFile("School_1_students");
		school2.addStudentFromFile("School_2_students");
		school3.addStudentFromFile("School_3_students");
		uni1.addStudentFromFile("Uni_1_students");
		uni2.addStudentFromFile("Uni_2_students");

		// Creating a list of the facility references
		LinkedList<Facility> facilityList = new LinkedList<Facility>(facilities.values());

		// Total income
		double totalIncome = 0.0;
		for (Facility facility : facilityList) 
		{
			totalIncome += facility.getIncome();
		}
		System.out.println("\nTotal income: " + String.format(Locale.US, "%.2f", totalIncome));

		// Average of all students per school/university
		System.out.println("\nAverage of all students per school/university: ");
		for (Facility facility : facilityList) 
		{
			System.out.println(facility.name + " - " + facility.averageGrade());
		}

		LinkedList<Student> allStudents = new LinkedList<Student>();
		for (Facility facility : facilityList) 
		{
			allStudents.addAll(facility.students);
		}

		// Top contributor to the total income
		System.out.println("\nTop contributor to the total income: ");
		Collections.sort(allStudents, Comparator.comparing(Student::calculatePayment).reversed());
		System.out.println(allStudents.getFirst());
		
		// Top performing student among all schools/universities, best male and female student
		Collections.sort(allStudents, Comparator.comparing(Student::getAverageGrade).reversed());
		Student bestStudent = allStudents.getFirst();
		System.out.println("\nBest " + bestStudent.getGender() + " student/Top performing student:\n" + bestStudent);
		allStudents.removeIf(student -> student.getGender() == bestStudent.getGender());
		System.out.println("\nBest " + allStudents.getFirst().getGender() + " student:\n" + allStudents.getFirst());
	}
}
