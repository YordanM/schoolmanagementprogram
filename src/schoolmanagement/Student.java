package schoolmanagement;

import java.util.Locale;
import java.util.Random;

enum Gender { male, female }

public class Student 
{
	private String name;
	private int age;
	private Gender gender;
	private double averageGrade;
	private int entityID;

	public Student(String name, int age, Gender gender, int entityID) 
	{
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.entityID = entityID;
		Random rand = new Random();
		double grade = 2 + rand.nextInt(4) + rand.nextDouble();
		this.averageGrade = Double.valueOf(String.format(Locale.US, "%.2f", grade));
	}

	public String toString() 
	{
		return "Name: " + this.name + "\nAge: " + this.age + "\nGender: " + this.gender + "\nAverage grade: "
				+ this.averageGrade + "\nEntity ID: " + this.entityID;
	}

	public double calculatePayment() 
	{
		Facility facility = MainClass.facilities.get(this.entityID);
		return (this.age / facility.averageGrade()) * 100 + facility.getTax();
	}

	public double getAverageGrade() 
	{
		return this.averageGrade;
	}

	public Gender getGender() 
	{
		return this.gender;
	}
}
