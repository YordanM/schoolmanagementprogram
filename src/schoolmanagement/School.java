package schoolmanagement;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class School extends Facility 
{
	private static int tax = 50;

	public School(int id, String name, String address) 
	{
		this.id = id;
		this.name = name;
		this.address = address;
		MainClass.facilities.put(id, this);
	}

	public void addStudent(String name, int age, Gender gender) 
	{
		students.add(new Student(name, age, gender, this.id));
		System.out.println("Hello " + name + " and welcome to " + this.name);
	}

	public void addStudentFromFile(String fileName) 
	{
		Scanner sc = null;
		try 
		{
			sc = new Scanner(new File(fileName));
			while (sc.hasNextLine()) 
			{
				String studentName = sc.next();
				students.add(new Student(studentName, sc.nextInt(), Gender.valueOf(sc.next()), this.id));
				System.out.println("Hello " + studentName + " and welcome to " + this.name);
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.getMessage();
		} 
		finally 
		{
			if (sc != null)
				sc.close();
		}
	}

	public double averageGrade() 
	{
		double grade = 0.0;
		for (Student student : students)
		grade += student.getAverageGrade();
		return Math.round(grade / students.size() * 100) / 100d;
	}

	public int getTax() {
		return School.tax;
	}

	public double getIncome() {
		double income = 0.0;
		for (Student student : this.students)
		income += student.calculatePayment();
		return income;
	}
}
